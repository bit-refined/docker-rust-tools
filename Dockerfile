FROM rust:latest
LABEL maintainer="Sophie Tauchert <sophie@999eagle.moe>"

ENV TARGET_LINUX=x86_64-unknown-linux-gnu \
	TARGET_WIN=x86_64-pc-windows-gnu
RUN mkdir -p ./build

# Install packages
COPY packages.sh ./
RUN ./packages.sh && \
	rm packages.sh

# Install kcov
COPY install_kcov.sh ./
RUN ./install_kcov.sh && \
	rm install_kcov.sh

# Install toolchains
COPY install.sh ./
RUN ./install.sh && \
	rm install.sh

# Clean up image
COPY cleanup.sh ./
RUN ./cleanup.sh && \
	rm cleanup.sh
