#!/usr/bin/env bash

set -e

TMPDIR=$(mktemp -d)
KCOV_VERSION=36

curl -Lo "$TMPDIR/kcov.tar.gz" "https://github.com/SimonKagstrom/kcov/archive/v${KCOV_VERSION}.tar.gz"
tar -xzf "$TMPDIR/kcov.tar.gz" -C $TMPDIR
mkdir -p "$TMPDIR/kcov-$KCOV_VERSION/build"
cd "$TMPDIR/kcov-$KCOV_VERSION/build"
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr ..
make
make install
rm -rf $TMPDIR
