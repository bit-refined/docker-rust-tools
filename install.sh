#!/usr/bin/env bash

set -e

# Add Windows target
rustup target add ${TARGET_WIN}
# Install components
rustup component add rustfmt clippy

# Install cargo tools
cargo install cargo-update
cargo install-update --all
cargo install cargo-outdated cargo-audit cargo-kcov cargo-geiger honggfuzz cargo-sweep
