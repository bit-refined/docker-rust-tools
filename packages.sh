#!/usr/bin/env bash

set -e

apt_packages=()

# required for honggfuzz
apt_packages+=(build-essential binutils-dev libunwind-dev libblocksruntime-dev)

# required for kcov
apt_packages+=(binutils-dev libcurl4-openssl-dev zlib1g-dev libdw-dev libiberty-dev)

# required for cargo-tarpaulin
apt_packages+=(libssl-dev pkg-config cmake zlib1g-dev)

# required for criterion bench reports
apt_packages+=(gnuplot)

# required for amethyst
apt_packages+=(libasound2-dev libx11-xcb-dev libssl-dev cmake libfreetype6-dev libexpat1-dev libxcb1-dev)

# required for windows toolchain
apt_packages+=(mingw-w64)

# required for pycobertura
apt_packages+=(python python-pip)

# misc stuff
apt_packages+=(jq)

# filter to unique elements
apt_packages=($(printf "%s\n" "${apt_packages[@]}" | sort -u))

# install packages
apt-get update
apt-get -y upgrade
apt-get -y install ${apt_packages[*]}

# install non-apt packages
pip install pycobertura
