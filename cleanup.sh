#!/usr/bin/env bash

set -e

# Remove source code of installed packages
rm -r $CARGO_HOME/registry
# Remove unneccessary toolchain files
rm -r $RUSTUP_HOME/toolchains/*/share
# Remove pip cache
rm -r $HOME/.cache/pip
# Remove apt lists
rm -r /var/lib/apt/lists
